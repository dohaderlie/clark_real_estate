$(document).ready(function() {
    $('form').submit(function(event) {
        var formData = {
            'fname'          : $('input[name=fname]').val(),
            'lname'          : $('input[name=lname]').val(),
            'email'          : $('input[name=email]').val(),
            'number'         : $('input[name=phone]').val(),
            'topic'          : $('select[name=topic]').val(),
            'comments'       : $('textarea[name=comments]').val()
        };

        $.ajax({
            type        : 'POST',
            url         : 'process.php',
            data        : 'formData',
            dataType    : 'json',
            encode      : true
        })
            .done(function(data) {

                console.log(data);

                if(!data.success) {

                    if (data.errors.name) {
                        $('#fname-group').addClass('has-error');
                        $('#fname-group').append('<div class="help-block">' + data.errors.name + '</div>');
                    }

                    if (data.errors.email) {
                        $('#lname-group').addClass('has-error');
                        $('#lname-group').append('<div class="help-block">' + data.errors.name + '</div>');
                    }

                    if (data.errors.name) {
                        $('#email-group').addClass('has-error');
                        $('#email-group').append('<div class="help-block">' + data.errors.name + '</div>');
                    }

                    if (data.errors.name) {
                        $('#phone-group').addClass('has-error');
                        $('#phone-group').append('<div class="help-block">' + data.errors.name + '</div>');
                    }

                    if (data.errors.name) {
                        $('#topic-group').addClass('has-error');
                        $('#topic-group').append('<div class="help-block">' + data.errors.name + '</div>');
                    }

                    if (data.errors.name) {
                        $('#comments-group').addClass('has-error');
                        $('#comments-group').append('<div class="help-block">' + data.errors.name + '</div>');
                    }
                } else {
                    $('form').append('<div class="alert alert-success">' + data.message + '</div>');

                    alert('success');
                }

            });

        event.preventDefault();

    });
});

exec_team = {
    change : function (div_id) {
        $(".exec_card").hide();
        $("." + div_id).fadeIn("slow");
    },
    bind : function () {
        $(".execImage").click(function () {
            exec_team.change($(this).data("exec"));
        }).attr("style","cursor:pointer;");
    }
};
exec_team.bind();
exec_team.change("gary");