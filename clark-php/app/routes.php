<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@index');
Route::get('/agent', 'HomeController@agent');
Route::get('/mls', 'HomeController@mls');
Route::get('/properties', 'HomeController@properties');
Route::get('/commercial', 'HomeController@commercial');
Route::get('/rentals', 'HomeController@rentals');
Route::get('/faqs', 'HomeController@faqs');
Route::get('/relocation', 'HomeController@relocation');
Route::get('/utah', 'HomeController@utah');
Route::get('/documents', 'HomeController@documents');
Route::get('/contact', 'HomeController@contact');
Route::get('/home', 'HomeController@index');
Route::get('/properties', 'HomeController@properties');
