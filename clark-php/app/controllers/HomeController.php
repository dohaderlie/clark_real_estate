<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('page.index');
	}

    public function agent()
    {
        return View::make('page.agent');
    }

    public function mls()
    {
        return View::make('page.mls');
    }

    public function properties()
    {
        return View::make('page.properties');
    }

    public function commercial()
    {
        return View::make('page.commercial');
    }

    public function rentals()
    {
        return View::make('page.rentals');
    }

    public function faqs()
    {
        return View::make('page.faqs');
    }

    public function relocation()
    {
        return View::make('page.relocation');
    }

    public function utah()
    {
        return View::make('page.utah');
    }

    public function documents()
    {
        return View::make('page.documents');
    }

    public function contact()
    {
        return View::make('page.contact');
    }

}
