<h3 class="relocate-h3"><b>General Information</b></h3>
<a name="general"></a>
<h6 class="relocate-h6"><b>Idaho the 43rd State</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Capital</td>
            <td class="relocate-td">Boise</td>
        </tr>
        <tr>
            <td class="relocate-td">Area</td>
            <td class="relocate-td">83,557 square miles</td>
        </tr>
        <tr>
            <td class="relocate-td">Elevations</td>
            <td class="relocate-td">12,662 High, 738 Low</td>
        </tr>
        <tr>
            <td class="relocate-td">State Horse</td>
            <td class="relocate-td">Appaloosa</td>
        </tr>
        <tr>
            <td class="relocate-td">State Bird</td>
            <td class="relocate-td">Mountain Bluebird</td>
        </tr>
        <tr>
            <td class="relocate-td">State Fossil</td>
            <td class="relocate-td">Hagerman Horse</td>
        </tr>
        <tr>
            <td class="relocate-td">State Tree</td>
            <td class="relocate-td">Western Pine</td>
        </tr>
        <tr>
            <td class="relocate-td">State Fish</td>
            <td class="relocate-td">Cutthroat Trout</td>
        </tr>
        <tr>
            <td class="relocate-td">State Flower</td>
            <td class="relocate-td">Syringa</td>
        </tr>
        <tr>
            <td class="relocate-td">State Fruit</td>
            <td class="relocate-td">Wild Huckleberry</td>
        </tr>
        <tr>
            <td class="relocate-td">State Folk Dance</td>
            <td class="relocate-td">Square Dance</td>
        </tr>
        <tr>
            <td class="relocate-td">State Gemstone</td>
            <td class="relocate-td">Star Garnet</td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>Mileage From Pocatello</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Blackfoot, ID</td>
            <td class="relocate-td">24</td>
        </tr>
        <tr>
            <td class="relocate-td">Boise, ID</td>
            <td class="relocate-td">234</td>
        </tr>
        <tr>
            <td class="relocate-td">Idaho Falls, ID</td>
            <td class="relocate-td">51</td>
        </tr>
        <tr>
            <td class="relocate-td">Missoula, MT</td>
            <td class="relocate-td">363</td>
        </tr>
        <tr>
            <td class="relocate-td">Moscow, ID</td>
            <td class="relocate-td">537</td>
        </tr>
        <tr>
            <td class="relocate-td">Portland, OR</td>
            <td class="relocate-td">732</td>
        </tr>
        <tr>
            <td class="relocate-td">Salt Lake, UT</td>
            <td class="relocate-td">161</td>
        </tr>
        <tr>
            <td class="relocate-td">San Francisco, CA</td>
            <td class="relocate-td">789</td>
        </tr>
        <tr>
            <td class="relocate-td">Seattle, WA</td>
            <td class="relocate-td">734</td>
        </tr>
        <tr>
            <td class="relocate-td">Spokane, WA</td>
            <td class="relocate-td">504</td>
        </tr>
        <tr>
            <td class="relocate-td">West Yellowstone, MT</td>
            <td class="relocate-td">159</td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>Voter Qualifications and Registration</b></h6>
<p class="relocate-p">An Idaho voter must be a citizen of the United States, at least 18 years of age on Election Day, a resident in the state, and in the county for thirty days prior to Election Day Registered as required by law.  Any county clerk or official shall register applicant up until twenty-five days preceding any election held throughout the country in which the applicant resides.</p>
<h3 class="relocate-h3"><b>Important Phone Numbers</b></h3>
<h6 class="relocate-h6"><b>Newspapers</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Idaho State Journal</td>
            <td class="relocate-td"><a href="tel:2082326150">(208) 232-6150</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Idaho Unido</td>
            <td class="relocate-td"><a href="tel:2082347383">(208) 234-7383</a></td>
        </tr>
        <tr>
            <td class="relocate-td">The Morning News</td>
            <td class="relocate-td"><a href="tel:2087851100">(208) 785-1100</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>City of Blackfoot</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Building</td>
            <td class="relocate-td"><a href="tel:2087858623">(208) 785-8623</a></td>
        </tr>
        <tr>
            <td class="relocate-td">City Hall</td>
            <td class="relocate-td"><a href="tel:2087858600">(208) 785-8600</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Fire</td>
            <td class="relocate-td"><a href="tel:2087858605">(208) 785-8605</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Garbabe</td>
            <td class="relocate-td"><a href="tel:2087858620">(208) 785-8620</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Groveland Water and Sewer</td>
            <td class="relocate-td"><a href="tel:2087858638">(208) 785-8638</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Sewer</td>
            <td class="relocate-td"><a href="tel:2087858616">(208) 785-8616</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Water</td>
            <td class="relocate-td"><a href="tel:2087858608">(208) 785-8608</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>City of Chubbuck</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Building Permits and Inspections</td>
            <td class="relocate-td"><a href="tel:2082372430">(208) 237-2430</a></td>
        </tr>
        <tr>
            <td class="relocate-td">City Clerk</td>
            <td class="relocate-td"><a href="tel:2082372400">(208) 237-2400</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Fire</td>
            <td class="relocate-td"><a href="tel:2082372430">(208) 237-2430</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Refuse</td>
            <td class="relocate-td"><a href="tel:2082372400">(208) 237-2400</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Planning and Zoning</td>
            <td class="relocate-td"><a href="tel:2082372430">(208) 237-2430</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Public Works</td>
            <td class="relocate-td"><a href="tel:2082372430">(208) 237-2430</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>City of Pocatello</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Building Department</td>
            <td class="relocate-td"><a href="tel:2082346158">(208) 234-6158</a></td>
        </tr>
        <tr>
            <td class="relocate-td">City Clerk</td>
            <td class="relocate-td"><a href="tel:2082346215">(208) 234-6215</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Fire</td>
            <td class="relocate-td"><a href="tel:2082346201">(208) 234-6201</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Refuse</td>
            <td class="relocate-td"><a href="tel:2082346192">(208) 234-6192</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Planning</td>
            <td class="relocate-td"><a href="tel:2082346161">(208) 234-6161</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Sanitation</td>
            <td class="relocate-td"><a href="tel:2082346241">(208) 234-6241</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>County of Bannock</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Clerk and Recorder</td>
            <td class="relocate-td"><a href="tel:2082367340">(208) 236-7340</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Drivers License</td>
            <td class="relocate-td"><a href="tel:2082367258">(208) 236-7258</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Motor Vehicle</td>
            <td class="relocate-td"><a href="tel:2082367200">(208) 236-7200</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Planning</td>
            <td class="relocate-td"><a href="tel:2082367230">(208) 236-7230</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>County of Bingham</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Clerk and Recorder</td>
            <td class="relocate-td"><a href="tel:2087823163">(208) 782-3163</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Motor Vehicle</td>
            <td class="relocate-td"><a href="tel:2087823031">(208) 782-3031</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Planning</td>
            <td class="relocate-td"><a href="tel:2087823177">(208) 782-3177</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>Commerce and Labor</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Blackfoot</td>
            <td class="relocate-td"><a href="tel:2082366713">(208) 236-6713</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Pocatello</td>
            <td class="relocate-td"><a href="tel:2082366710">(208) 236-6710</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>Emergency</b></h6>
<p class="relocate-p">911</p>
<h6 class="relocate-h6"><b>Resources</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Blackfoot Chamber or Commerce</td>
            <td class="relocate-td"><a href="tel:2087850510">(208) 785-0510</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Pocatello Chamber of Commerce</td>
            <td class="relocate-td"><a href="tel:2082331525">(208) 233-1525</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Convention and Visitors Bureau Pocatello</td>
            <td class="relocate-td"><a href="tel:2082346184">(208) 234-6184</a></td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>Refuse Collection and Utilities</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Psi Environmental Services</td>
            <td class="relocate-td"><a href="tel:2082340056">(208) 234-0056</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Snake River Dispose-All</td>
            <td class="relocate-td"><a href="tel:2086343404">(208) 634-3404</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Intermountain Gas Co. Pocatello</td>
            <td class="relocate-td"><a href="tel:2086376400">(208) 637-6400</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Intermountain Gas Co. Blackfoot</td>
            <td class="relocate-td"><a href="tel:8005483679">(800) 548-3679</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Idaho Power (Electric)</td>
            <td class="relocate-td"><a href="tel:2083882200">(208) 388-2200</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Rocky Mountain Power (Electric)</td>
            <td class="relocate-td"><a href="tel:8882217070">(888) 221-7070</a></td>
        </tr>
        <tr>
            <td class="relocate-td">Qwest</td>
            <td class="relocate-td"><a href="tel:8002441111">(800) 244-1111</a></td>
        </tr>
    </tbody>
</table>