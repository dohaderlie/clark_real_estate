<nav class="navbar-left">
    <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav nav-pills">
                <li><a href="#welcome">Welcome</a></li><br/>
                <li><a href="#pocatello">Pocatello</a></li><br/>
                <li><a href="#education">Education</a></li><br/>
                <li><a href="#chubbuck">Chubbuck</a></li><br/>
                <li><a href="#recreation">Attractions and Recreation</a></li><br/>
                <li><a href="#general">General Information</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>