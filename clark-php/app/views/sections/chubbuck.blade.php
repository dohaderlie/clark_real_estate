<h3 class="relocate-h3"><b>Chubbuck</b></h3>
<a name="chubbuck"></a>
<p class="relocate-p">Chubbuck is a progressive community of approximately 10,000 highly motivated residents.  Chubbuck is named after an English railroad conductor, Earl Chubbuck, and has grown to be the 14th largest city in Idaho since incorporating in 1949.  It is strongly associated with Pocatello, a short drive to the south.  Chubbuk is an intrical part of the medical community, shopping and cultural activities, recreation, and transpiring in the area.  Home to the Pine Ridge Mall, Chubbuck has become a regional shopping hub.  There are a number of retirement and assisted living centers to be found in this quiet community.  The city has a comprehensive plan to develop new commercial and light industrial businesses.  A greater majority of the neighborhoods have schools, churches, and recreational facilities within walking distance.</p>
<h6 class="relocate-h6"><b>Climate and Topography</b></h6>
<table class="relocate-table">
    <tr>
        <td class="relocate-td">Elevation</td>
        <td class="relocate-td">4,448 feet</td>
    </tr>
    <tr>
        <td class="relocate-td">Precipitation</td>
        <td class="relocate-td">Less than 15 inches annually</td>
    </tr>
    <tr>
        <td class="relocate-td">Average Summer Temperature</td>
        <td class="relocate-td">72.4 degrees</td>
    </tr>
    <tr>
        <td class="relocate-td">Average Winter Temperature</td>
        <td class="relocate-td">35.4 degrees</td>
    </tr>
</table>
<h6 class="relocate-h6"><b>Chubbuck Population</b></h6>
<table class="relocate-table">
    <tr>
        <td class="relocate-td">Chubbuck City</td>
        <td class="relocate-td">14,067</td>
    </tr>
    <tr>
        <td class="relocate-td">Square Miles in County</td>
        <td class="relocate-td">4.19</td>
    </tr>
</table>
<h6 class="relocate-h6"><b>Education</b></h6>
<p class="relocate-p">Chubbuck are part of the Pocatello School District.  Click <a href="">here</a> to go to the section on School District #25.</p>