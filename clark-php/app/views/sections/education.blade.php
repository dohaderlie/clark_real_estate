<h3 class="relocate-h3"><b>Education</b></h3>
<a name="education"></a>
<p class="relocate-p">Pocatello's School District has a solid reputation when it comes to quality of education, grades K-12.  they are nationally recognized for their adult education classes.  Pocatello's School District is more progressive than the average school district:.  They have implemented a Teen Parenting Program and an Evening High School program for students with special needs.</p>
<h6 class="relocate-h6"><b>Pocatello/Chubbuck School District 25</b></h6>
<p class="relocate-p">3115 Pole Line Rd.<br/>
    Pocatello, ID 83201<br/>
    <a href="tel:2082323563">(208) 232-3563</a><br/>
    <a href="www.d25.k12.id.us">www.d25.k12.id.us</a></p>
<h6 class="relocate-h6"><b>Charter Schools</b></h6>
<p class="relocate-p">Pocatello Community Charter School - <a href="tel:2084782522">(208) 478-2522</a></p>
<h6 class="relocate-h6"><b>Private Schools</b></h6>
<ul class="relocate-list">
    <li>Academy at Roosevelt Center - <a href="tel:2082321447">(208) 232-1447</a></li>
    <li>Calvary Chapel Christian School - <a href="tel:2082379500">(208) 237-9500</a></li>
    <li>Franciscan Cre-act School - <a href="tel:2082334747">(208) 233-4747</a></li>
    <li>Grace Lutheran School - <a href="tel:2082374142">(208) 237-4142</a></li>
    <li>Holy Spirit Catholic School - <a href="tel:2082325763">(208) 232-5763</a></li>
    <li>Pocatello Christian School - <a href="tel:2082518641">(208) 251-8641</a></li>
    <li>Jessie Clark Christian School - <a href="tel:2082375309">(208) 237-5309</a></li>
    <li>ISU Early Learning Center - <a href="tel:2082822769">(208) 282-2769</a></li>
</ul>
<h6 class="relocate-h6"><b>Idaho State University (ISU)</b></h6>
<p class="relocate-p">921 South 8th Ave.<br/>
    Pocatello, ID 83204<br/>
    <a href="tel:2082820211">(208) 282-0211</a><br/>
    <a href="www.isu.edu">www.isu.edu</a></p>
<p class="relocate-p">ISU is amont the fastest growing four year academic educational institutions supported by the State of Idaho.  Originally founded as the Academy of Idaho, ISU celebrated its 100th year of existence in 2001.  The College of Arts and Sciences, Pharmacy, Health-Related Professions, Education, Engineering and Business host over 14,000 students who participate in a Bachelor's Master's and Doctoral Degrees, there are over 255 different degree programs.  ISU is the only university in the United States to allow Shoshone as a general education requirement.  In addition ISU has an outstanding school of applied technology offering classes and opportunities to attain a certificate in several technical and career-oriented fields.</p>
<p class="relocate-p">One of the university's newest departments on campus is the Accelerator Center.  The Idaho Accelerator Center is a partnership between the University of Idaho and the U.S. Department of Energy and Environmental Laboratory (INL).  The center conducts research and provides students with training in radiation science and low-energy charged-particle accelerators.  ISU is one of seven universities that facilitates this new research and educational partnership that is co-managed by the Idaho National Laboratory at INL (208) 282-0211.</p>
<p class="relocate-p">Idaho State University is home to the L.E. and Thelma E. Stephens Performing Arts Center.  The Center is a world class facility with theaters, a 1,200 seat, state of the art grand concert hall, a central rotunda, offices and classrooms.  The campus also houses the Idaho Museum of Natural History.  This is a state museum holding permanent collections of Idaho artifact and traveling exhibits of current interest.</p>
<p class="relocate-p">Holt Arena, with a capacity of approximately 1,200 hosts major athletic events, concerts, trade shows, and the Dodge National Finals Circuit Rodeo.  Holt Arena is the home of ISU Athletics; it is the largest facility in eastern Idaho.</p>