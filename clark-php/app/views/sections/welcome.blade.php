<h3 class="relocate-h3"><b>Welcome To Southeast Idaho!</b></h3>
<a name="welcome"></a>
<p class="relocate-p">Southeast Idaho is made up of the cities of Pocatello, Chubbuck, Blackfoot, American Falls and Inkom.  These communities are located off of Interstate 15 and Interstate 86, the crossroads to Salt Lake City, Boise, Yellowstone, the Grand Teton National Parks, Sun Valley and a multitude of other important destinations.  The area is rich in history, education, agriculture, industry, tourism and recreation.  The Snake River and Portneuf River run through this magnificent area.</p>
<h6 class="relocate-h6">Climate and Topography</h6>
<p class="relocate-p">Nestled in the foothills of the Rocky Mountains 4,448 feet above sea level.  Clear, dry and sunny days are normal for this region of Idaho.  This desirable four-season area receives less than 15 inches of precipitation annually, summers are dry and comfortably warm.  The average temperateure in the summer is 72 degrees, while the average temperature in the winter is 35 degrees, an average swing of 37 degrees.</p>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Average Summer Temperature</td>
            <td class="relocate-td">87.5&deg;</td>
        </tr>
        <tr>
            <td class="relocate-td">Average Winter Temperature</td>
            <td class="relocate-td">32.5&deg;</td>
        </tr>
        <tr>
            <td class="relocate-td">Annual Precipitation</td>
            <td class="relocate-td">Less than 15 inches</td>
        </tr>
        <tr>
            <td class="relocate-td">Elevation</td>
            <td class="relocate-td">4,448 Feet</td>
        </tr>
    </tbody>
</table>