<h4 class="subcategories">Areas of Pocatello</h4>
<p class="info">Real estate in the Pocatello metro region is generally organized into seven major areas. To learn about the characteristics of each area, click the links below.</p>
<p class="info">Real estate in the Pocatello area is usually split into seven major areas.  Those areas are broken down for you below.</p>
<h5 class="area">Buckskin - </h5><p class="info">rustic living and privacy in the canyons and foothills of the east side.  This is a popular area for vacation homes in the Pocatello area.</p>
<p class="info">Below are real estate statistics from the Pocatllo MLS.  These statistics are based on the properties that are being sold in the Buckskin area.</p>
<table class="table table-hover table-responsive">
    <thead>
    <tr>
        <th> </th>
        <th><b>2014<br/>Pocatello<br/>Average</b></th>
        <th>2014<br/>Buckskin<br/>Average</th>
        <th>2013<br/>Buckskin<br/>Average</th>
        <th>2012<br/>Buckskin<br/>Average</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Sold Price</th>
        <td><b>$152,547</b></td>
        <td>$196,813</td>
        <td>$289,813</td>
        <td>$326,982</td>
    </tr>
    <tr>
        <th scope="row">Square Feet</th>
        <td><b>2,137</b></td>
        <td>2,355</td>
        <td>3,202</td>
        <td>3,496</td>
    </tr>
    <tr>
        <th scope="row">Year Built</th>
        <td><b>1973</b></td>
        <td>1986</td>
        <td>1991</td>
        <td>1979</td>
    </tr>
    <tr>
        <th scope="row">Bedrooms</th>
        <td><b>3.5</b></td>
        <td>3.6</td>
        <td>3.8</td>
        <td>4.6</td>
    </tr>
    <tr>
        <th scope="row">Full Bathrooms</th>
        <td><b>1.9</b></td>
        <td>2.0</td>
        <td>2.4</td>
        <td>2.7</td>
    </tr>
    <tr>
        <th scope="row">Garage Stalls</th>
        <td><b>1.6</b></td>
        <td>2.3</td>
        <td>2.5</td>
        <td>2.6</td>
    </tr>
    <tr>
        <th scope="row">Days on Market</th>
        <td><b>127</b></td>
        <td>141</td>
        <td>105</td>
        <td>113</td>
    </tr>
    </tbody>
</table>
<h5 class="area">Chubbuck - </h5><p class="info">Adjoins Pocatello on the North side.  Chubbuck is a friendly community with about 14,000 people.  Although Chubbuck is growing you can still find flat acreage in a lot of places in Chubbuck. </p>
<p class="info">Below are real estate statistics from the Pocatllo MLS.  These statistics are based on the properties that are being sold in the Chubbuck area.</p>
<table class="table table-hover table-responsive">
    <thead>
    <tr>
        <th> </th>
        <th><b>2014<br/>Pocatello<br/>Average</b></th>
        <th>2014<br/>Chubbuck<br/>Average</th>
        <th>2013<br/>Chubbuck<br/>Average</th>
        <th>2012<br/>Chubbuck<br/>Average</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Sold Price</th>
        <td><b>$152,547</b></td>
        <td>$156,980</td>
        <td>$170,313</td>
        <td>$159,972</td>
    </tr>
    <tr>
        <th scope="row">Square Feet</th>
        <td><b>2,137</b></td>
        <td>2,146</td>
        <td>2,246</td>
        <td>2,099</td>
    </tr>
    <tr>
        <th scope="row">Year Built</th>
        <td><b>1973</b></td>
        <td>1993</td>
        <td>1994</td>
        <td>1995</td>
    </tr>
    <tr>
        <th scope="row">Bedrooms</th>
        <td><b>3.5</b></td>
        <td>3.4</td>
        <td>3.6</td>
        <td>3.5</td>
    </tr>
    <tr>
        <th scope="row">Full Bathrooms</th>
        <td><b>1.9</b></td>
        <td>2.0</td>
        <td>2.0</td>
        <td>2.0</td>
    </tr>
    <tr>
        <th scope="row">Garage Stalls</th>
        <td><b>1.6</b></td>
        <td>1.9</td>
        <td>1.9</td>
        <td>1.9</td>
    </tr>
    <tr>
        <th scope="row">Days on Market</th>
        <td><b>127</b></td>
        <td>132</td>
        <td>109</td>
        <td>109</td>
    </tr>
    </tbody>
</table>
<h5 class="area">Highland - </h5><p class="info">located on the east bench of town, you will experience magnificent views of the city.  This area is popular for it's tree-lined neighborhoods and the coming Portneuf Wellness Complex! </p>
<p class="info">Below are real estate statistics from the Pocatllo MLS.  These statistics are based on the properties that are being sold in the Highland area.</p>
<table class="table table-hover table-responsive">
    <thead>
    <tr>
        <th> </th>
        <th><b>2014<br/>Pocatello<br/>Average</b></th>
        <th>2014<br/>Highland<br/>Average</th>
        <th>2013<br/>Highland<br/>Average</th>
        <th>2012<br/>Highland<br/>Average</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Sold Price</th>
        <td><b>$152,547</b></td>
        <td>$196,813</td>
        <td>$289,813</td>
        <td>$326,982</td>
    </tr>
    <tr>
        <th scope="row">Square Feet</th>
        <td><b>2,137</b></td>
        <td>2,355</td>
        <td>3,202</td>
        <td>3,496</td>
    </tr>
    <tr>
        <th scope="row">Year Built</th>
        <td><b>1973</b></td>
        <td>1986</td>
        <td>1991</td>
        <td>1979</td>
    </tr>
    <tr>
        <th scope="row">Bedrooms</th>
        <td><b>3.5</b></td>
        <td>3.6</td>
        <td>3.8</td>
        <td>4.6</td>
    </tr>
    <tr>
        <th scope="row">Full Bathrooms</th>
        <td><b>1.9</b></td>
        <td>2.0</td>
        <td>2.4</td>
        <td>2.7</td>
    </tr>
    <tr>
        <th scope="row">Garage Stalls</th>
        <td><b>1.6</b></td>
        <td>2.3</td>
        <td>2.5</td>
        <td>2.6</td>
    </tr>
    <tr>
        <th scope="row">Days on Market</th>
        <td><b>127</b></td>
        <td>141</td>
        <td>105</td>
        <td>113</td>
    </tr>
    </tbody>
</table>
<h5 class="area">North Pocatello - </h5><p class="info">is where the majority of Pocatello's commercial district lies.  So if you enjoy shopping and being out on the town this is the place to be.</p>
<p class="info">Below are real estate statistics from the Pocatllo MLS.  These statistics are based on the properties that are being sold in the North Pocatello area.</p>
<table class="table table-hover table-responsive">
    <thead>
    <tr>
        <th> </th>
        <th><b>2014<br/>Pocatello<br/>Average</b></th>
        <th>2014<br/>North Pocatello<br/>Average</th>
        <th>2013<br/>North Pocatello<br/>Average</th>
        <th>2012<br/>North Pocatello<br/>Average</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Sold Price</th>
        <td><b>$152,547</b></td>
        <td>$196,813</td>
        <td>$289,813</td>
        <td>$326,982</td>
    </tr>
    <tr>
        <th scope="row">Square Feet</th>
        <td><b>2,137</b></td>
        <td>2,355</td>
        <td>3,202</td>
        <td>3,496</td>
    </tr>
    <tr>
        <th scope="row">Year Built</th>
        <td><b>1973</b></td>
        <td>1986</td>
        <td>1991</td>
        <td>1979</td>
    </tr>
    <tr>
        <th scope="row">Bedrooms</th>
        <td><b>3.5</b></td>
        <td>3.6</td>
        <td>3.8</td>
        <td>4.6</td>
    </tr>
    <tr>
        <th scope="row">Full Bathrooms</th>
        <td><b>1.9</b></td>
        <td>2.0</td>
        <td>2.4</td>
        <td>2.7</td>
    </tr>
    <tr>
        <th scope="row">Garage Stalls</th>
        <td><b>1.6</b></td>
        <td>2.3</td>
        <td>2.5</td>
        <td>2.6</td>
    </tr>
    <tr>
        <th scope="row">Days on Market</th>
        <td><b>127</b></td>
        <td>141</td>
        <td>105</td>
        <td>113</td>
    </tr>
    </tbody>
</table>
<h5 class="area">South Pocatello - </h5><p class="info">has the most of the areas natural features.  This area offers lava formations, streams, rock-climbing cliffs, an aquatic complex, the city zoo, and the country club.</p>
<p class="info">Below are real estate statistics from the Pocatllo MLS.  These statistics are based on the properties that are being sold in the South Pocatello area.</p>
<table class="table table-hover table-responsive">
    <thead>
    <tr>
        <th> </th>
        <th><b>2014<br/>Pocatello<br/>Average</b></th>
        <th>2014<br/>South Pocatello<br/>Average</th>
        <th>2013<br/>South Pocatello<br/>Average</th>
        <th>2012<br/>South Pocatello<br/>Average</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Sold Price</th>
        <td><b>$152,547</b></td>
        <td>$196,813</td>
        <td>$289,813</td>
        <td>$326,982</td>
    </tr>
    <tr>
        <th scope="row">Square Feet</th>
        <td><b>2,137</b></td>
        <td>2,355</td>
        <td>3,202</td>
        <td>3,496</td>
    </tr>
    <tr>
        <th scope="row">Year Built</th>
        <td><b>1973</b></td>
        <td>1986</td>
        <td>1991</td>
        <td>1979</td>
    </tr>
    <tr>
        <th scope="row">Bedrooms</th>
        <td><b>3.5</b></td>
        <td>3.6</td>
        <td>3.8</td>
        <td>4.6</td>
    </tr>
    <tr>
        <th scope="row">Full Bathrooms</th>
        <td><b>1.9</b></td>
        <td>2.0</td>
        <td>2.4</td>
        <td>2.7</td>
    </tr>
    <tr>
        <th scope="row">Garage Stalls</th>
        <td><b>1.6</b></td>
        <td>2.3</td>
        <td>2.5</td>
        <td>2.6</td>
    </tr>
    <tr>
        <th scope="row">Days on Market</th>
        <td><b>127</b></td>
        <td>141</td>
        <td>105</td>
        <td>113</td>
    </tr>
    </tbody>
</table>
<h5 class="area">University - </h5><p class="info">Boasts the campus of Idaho State University.  ISU supports a healthy rental market, however, the area is popular with homeowners, especially to the north and east of campus.</p>
<p class="info">Below are real estate statistics from the Pocatllo MLS.  These statistics are based on the properties that are being sold in the University area.</p>
<table class="table table-hover table-responsive">
    <thead>
    <tr>
        <th> </th>
        <th><b>2014<br/>Pocatello<br/>Average</b></th>
        <th>2014<br/>University<br/>Average</th>
        <th>2013<br/>University<br/>Average</th>
        <th>2012<br/>University<br/>Average</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Sold Price</th>
        <td><b>$152,547</b></td>
        <td>$196,813</td>
        <td>$289,813</td>
        <td>$326,982</td>
    </tr>
    <tr>
        <th scope="row">Square Feet</th>
        <td><b>2,137</b></td>
        <td>2,355</td>
        <td>3,202</td>
        <td>3,496</td>
    </tr>
    <tr>
        <th scope="row">Year Built</th>
        <td><b>1973</b></td>
        <td>1986</td>
        <td>1991</td>
        <td>1979</td>
    </tr>
    <tr>
        <th scope="row">Bedrooms</th>
        <td><b>3.5</b></td>
        <td>3.6</td>
        <td>3.8</td>
        <td>4.6</td>
    </tr>
    <tr>
        <th scope="row">Full Bathrooms</th>
        <td><b>1.9</b></td>
        <td>2.0</td>
        <td>2.4</td>
        <td>2.7</td>
    </tr>
    <tr>
        <th scope="row">Garage Stalls</th>
        <td><b>1.6</b></td>
        <td>2.3</td>
        <td>2.5</td>
        <td>2.6</td>
    </tr>
    <tr>
        <th scope="row">Days on Market</th>
        <td><b>127</b></td>
        <td>141</td>
        <td>105</td>
        <td>113</td>
    </tr>
    </tbody>
</table>
<h5 class="area">West Pocatello - </h5><p class="info">is the area where Pocatello started.  The buildings and homes in this are date back to the 19th century.  This is a great area for people starting out and who have a smaller budget.</p>
<p class="info">Below are real estate statistics from the Pocatllo MLS.  These statistics are based on the properties that are being sold in the West Pocatello area.</p>
<table class="table table-striped table-responsive">
    <thead>
    <tr>
        <th> </th>
        <th><b>2014<br/>Pocatello<br/>Average</b></th>
        <th>2014<br/>West Pocatello<br/>Average</th>
        <th>2013<br/>West Pocatello<br/>Average</th>
        <th>2012<br/>West Pocatello<br/>Average</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Sold Price</th>
        <td><b>$152,547</b></td>
        <td>$196,813</td>
        <td>$289,813</td>
        <td>$326,982</td>
    </tr>
    <tr>
        <th scope="row">Square Feet</th>
        <td><b>2,137</b></td>
        <td>2,355</td>
        <td>3,202</td>
        <td>3,496</td>
    </tr>
    <tr>
        <th scope="row">Year Built</th>
        <td><b>1973</b></td>
        <td>1986</td>
        <td>1991</td>
        <td>1979</td>
    </tr>
    <tr>
        <th scope="row">Bedrooms</th>
        <td><b>3.5</b></td>
        <td>3.6</td>
        <td>3.8</td>
        <td>4.6</td>
    </tr>
    <tr>
        <th scope="row">Full Bathrooms</th>
        <td><b>1.9</b></td>
        <td>2.0</td>
        <td>2.4</td>
        <td>2.7</td>
    </tr>
    <tr>
        <th scope="row">Garage Stalls</th>
        <td><b>1.6</b></td>
        <td>2.3</td>
        <td>2.5</td>
        <td>2.6</td>
    </tr>
    <tr>
        <th scope="row">Days on Market</th>
        <td><b>127</b></td>
        <td>141</td>
        <td>105</td>
        <td>113</td>
    </tr>
    </tbody>
</table>