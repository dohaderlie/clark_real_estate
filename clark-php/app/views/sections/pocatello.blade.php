<h3 class="relocate-h3"><b>Pocatello</b></h3>
<a name="pocatello"></a>
<p class="relocate-p">Pocatello which is pronounced poke-uh-tell-o is the county seat of Bannock County and was founded as a railroad center in 1882.  Pocatello is located along the Portneuf River near the intersection of highways I-15 and I-86, half way between Salt Lake City, Yellowstone Park and Sun Valley.  The city is home to Idaho State University which provides an excellent educational base to the area as well as extending the cultural awareness of the area.  Nearby national forests offer an abundance of year round outdoor recreation, spectacular scenery and plentiful wildlife.  The city provides plentiful entertainment - county fairs, rodeos, top-notch college and high school athletics, and a multitude of outdoor activities.  Year round recreational activities are always available such as: golf, tennis, swimming, fishing, hunting, skiing, hiking or soaking in one of many hot springs.</p>
<p class="relocate-p">The Portneuf River runs through Pocatello and converges with the Snake River at the American Falls Reservoir.  The beautiful attractions in and around Pocatello brings the area a lot of tourism making it one of the areas top employers and the reason why the city is often referred to as \"The Gate City\"  The city of Pocatello still sits where it was originally built, it has just expanded around the original city.  Old Town Pocatello is an intricate part of the Portneuf Greenway.  The Greenway traverses along the Portneuf River for nearly 20 miles, with handicap access along the way.</p>
<p class="relocate-p">Pocatello is named after the Shoshone-Bannock Indian Chief Pocatello, who made available to the railroad a right-of-way through the Fort Hall Indian Reservation.  The Shoshone and Bannock Indian Tribes inhabited this area of Idaho long before Meriwether Lewis and William Clark arrived in 1805.  The Lewis and Clark Exposition opened this land for fur traders and trappers.  A few miles northeast of Pocatello lies Fort Hall, established in 1834, by the Hudson Bay Company.  Due to over trapping and a major change in the fashion world Fort Hall became a supply point for immigrants traveling the famed Oregon Trail. </p>
<p class="relocate-p">It was not the Gold Rush of 1860 that brought thousands of immigrants, who settled this portion of Idaho.  The Gold Rush developed a need for goods and services in the Portneuf Valley, home of Pocatello.  Initially stage lines and freight companies used this corridor.  As Idaho's mineral resources and Pocatello Junction were developed and became an important crossroad, the Union Pacific Railroad extended its service westward.  As gold began to diminish in 1882 the settlers who stayed began turning to agriculture.  This region became a major supplier of potatoes, grain and other crops with help of irrigation from the Snake River.</p>
<h6 class="relocate-h6"><b>Economy</b></h6>
<p class="relocate-p">Pocatello enjoys a stable and diversified economic base with the leading source of income in the area being agricultural processing and products, manufacturing, mining surrounding trade, agriculture, transportation, education, high tech, nuclear research and tourism.  An active economic development team headquartered at Idaho State University research and the Business Park are assets helping to keep the economy diverse.  The Government sector provides a quarter of the jobs in Bannock County because the area is home to Idaho National Laboratory (INL), Idaho State University, the Idaho Women's Correctional Facility, FBI Western Region Customer Support Center, Burequ of Land Management (BLM), US Forest Service, US Federal Court House, Idaho Fish and Game, school districts along with local, and city governments.</p>
<h6 class="relocate-h6"><b>Major Employers</b></h6>
<ul class="relocate-list">
    <li>Government, Including INL</li>
    <li>On Semiconductor, Inc</li>
    <li>Convergys Customer Management</li>
    <li>Farm Bureau Insurance</li>
    <li>Farmers Insurance</li>
    <li>Amy's Kitchen</li>
    <li>Idaho State University</li>
    <li>Portneuf Medical Center</li>
    <li>Beacon Health Services</li>
    <li>Union Pacific Railroad</li>
    <li>Belmont Care Center</li>
    <li>Varsity Conductors</li>
    <li>Walmart</li>
</ul>
<h6 class="relocate-h6"><b>Taxes</b></h6>
<p class="relocate-p">Property Taxes are limited to one percent of market value, against \"percentage of value,\" levies are applied.  At present, a $70,000 Pocatello home you will pay approximately $1,054 annually in property taxes, with homeowner's exemption.  The state of Idaho collects a 6% general retail sales tax, a personal income tax of 1.6-7.4% of taxable income, and a 7.7% corporate income tax.</p>
<h4 class="relocate-h4"><b>Transportation Services</b></h4>
<h6 class="relocate-h6"><b>Railroad</b></h6>
<p class="relocate-p">The Union Pacific Railroad supplies service to the southeastern portion of Idaho.  Pocatello has a history of railroad involvement thus many of the buildings have access to valuable railroad spurs.  New industry can be connected to the rail system with little or no effort.  Pocatello is the regional headquarters of the Union Pacific Railroad and houses the largest classification yard in the Intermountain West.</p>
<h6 class="relocate-h6"><b>Airlines</b></h6>
<p class="relocate-p">Pocatello Regional Airport is a full service airport: restaurant and Lounge, meeting rooms, auto rentals, full instrument system and air traffic control system, public parking as well as easy access to the interstate.  The airport is located 10 miles west of town off Hwy 86.  Sky West your Delta connection has a number of flights daily to Salt Lake City.</p>
<ul class="relocate-list">
    <li>Airport - <a href="tel:2082346154"></a> - <a href="www.iflypocatello.com">www.iflypocatello.com</a></li>
    <li>SkyWest - <a href="tel:2082211212">(208) 221-1212</a> - <a href="tel:2082346154">(208) 234-6154</a></li>
</ul>
<h6 class="relocate-h6"><b>Bus</b></h6>
<p class="relocate-p">GreyHound - <a href="tel:2082346248">(208) 234-6248</a></p>
<h6 class="relocate-h6"><b>Public Transportation</b></h6>
<ul class="relocate-list">
    <li>Pocatello Urban Transit - <a href="tel:2082346248">(208) 234-6248</a></li>
    <li>Operates 7 bus routes</li>
    <li>Charter service</li>
    <li>Serves ISU students</li>
    <li><a href="www.pocatellotransit.com">www.pocatellotransit.com</a></li>
</ul>
<h4 class="relocate-h4"><b>Housing Information</b></h4>
<h6 class="relocate-h6">Housing</h6>
<p class="relocate-p">Please check with your Realtor for updated housing information.</p>
<p class="relocate-p">Clark Real Estate<br/>
    <a href="www.clarkrealestateco.com">www.clarkrealestateco.com</a><br/>
    <a href="tel:2082332424">(208) 233-2424</a><br/></p>
<p class="relocate-p">Pocatello's cost of living is well below the national average for such items as groceries, housing, utilities, transportation and miscellaneous goods and services.</p>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Avg. Residential Listing Price in Pocatello</td>
            <td class="relocate-td">$160,131</td>
        </tr>
        <tr>
            <td class="relocate-td">Avg. Residential Listing Price in Bannock Co.</td>
            <td class="relocate-td">$160,008</td>
        </tr>
        <tr>
            <td class="relocate-td">Median Rent</td>
            <td class="relocate-td">$550-800</td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>Utilities</b></h6>
<p class="relocate-p">Average cost of residential electricity $71 (1,000KWH/mo.)</p>
<p class="relocate-p">Average cost of residential natural gas $85 (75 Therms/mo.)</p>
<p class="relocate-p">INTERMOUNTAIN GAS COMPANY - <a href="tel:2086376400">(208) 637-6400</a> - <a href="www.intgas.com">www.intgas.com</a></p>
<p class="relocate-p">IDAHO POWER - <a href="tel:2082367735">(208) 236-7735</a> - <a href="www.idahopower.com">www.idahopower.com</a></p>
<h6 class="relocate-h6"><b>Health Care</b></h6>
<p class="relocate-p">Modern health care and medical services in Pocatello include: Two regional acute care facilities, operating through the Portneuf Medical Center.  Both fully accredited and equipped with state-of-the-art diagnostic technology including: MRI, CT scanners, 24-hour emergency physician staffed care centers and specialized women's centers.</p>
<p class="relocate-p">Portneuf Medical Center is a not for profit facility owned by the citizens of Bannock County with approximately 1,300 employees.</p>
<p class="relocate-p">Portneuf Medical Center<br/>
    777 Hospital Way<br/>
    <a href="tel:2082391000">(208) 239-1000</a></p>
<h6 class="relocate-h6"><b>Retired Living and Senior Services</b></h6>
<p class="relocate-p">Pocatello is a great place to retire, in fact it was named as one of the top 10 places to retire.  In addition to comprehensive services, Pocatello offers good weather, friendly folk, accessible outdoor recreation, affordable housing and top-notch medical facilities. The community features a vibrant arts culture and further learning through Idaho State University.</p>
<p class="relocate-p">There is a full list of housing options including houses, apartments, condominiums, assisted living and nursing homes available at the Area Agency on Aging.  They can be reached at
    <a href="tel:2082334032">(208) 233-4032</a> or visit <a href="www.sicog.org">www.sicog.org</a></p>
<h6 class="relocate-h6"><b>Shopping and Dinning</b></h6>
<p class="relocate-p">Shopping and dining is plentiful in the community.  The Old Town historic district is home to many unique shops and eateries.  One can enjoy a leisurely day of shopping in the district or walking the greenway.  The Pine Ridge Mall located in Chubbuck has Herbergers, JC Penney, Sears, and Shopko as anchor stores.  Numerous other stores are located in the mall.</p>
<h6 class="relocate-h6"><b>Parks and Recreation</b></h6>
<p class="relocate-p">The Pocatello area has approximately 40 parks.  Parks offer an array of activities, from walking, biking, athletic fields, and tennis courts.  The Portneuf Greenway gives access to sites of the Portneuf River along the trail.  Ross Park is well known for its summer concerts, aquatic center, picnic area, and zoo.  the golf enthusiast can enjoy the sport in the area's two public golf courses and one private country club.</p>
<h4 class="relocate-h4"><b>Pocatello Statistics</b></h4>
<h6 class="relocate-h6"><b>Population</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Idaho</td>
            <td class="relocate-td">1,546,590</td>
        </tr>
        <tr>
            <td class="relocate-td">Pocatello</td>
            <td class="relocate-td">54,777</td>
        </tr>
        <tr>
            <td class="relocate-td">Bannock Co.</td>
            <td class="relocate-td">83,800</td>
        </tr>
    </tbody>
</table>
<h6 class="relocate-h6"><b>Income</b></h6>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td">Per Capita Personal Income</td>
            <td class="relocate-td">1996</td>
            <td class="relocate-td">2000</td>
            <td class="relocate-td">2011</td>
        </tr>
        <tr>
            <td class="relocate-td">Bannock County</td>
            <td class="relocate-td">$17,854</td>
            <td class="relocate-td">$20,856</td>
            <td class="relocate-td">$28,818</td>
        </tr>
        <tr>
            <td class="relocate-td">State of Idaho</td>
            <td class="relocate-td">$20,248</td>
            <td class="relocate-td">$24,073</td>
            <td class="relocate-td">$32,881</td>
        </tr>
        <tr>
            <td class="relocate-td">United States</td>
            <td class="relocate-td">$24,175</td>
            <td class="relocate-td">$29,843</td>
            <td class="relocate-td">$41,560</td>
        </tr>
    </tbody>
</table>
<table class="relocate-table">
    <tbody>
        <tr>
            <td class="relocate-td"><h6><b>Labor Force</b></h6></td>
            <td class="relocate-td">2000</td>
            <td class="relocate-td">2013</td>
        </tr>
        <tr>
            <td class="relocate-td">Total Labor Force</td>
            <td class="relocate-td">38,397</td>
            <td class="relocate-td">39,247</td>
        </tr>
        <tr>
            <td class="relocate-td">% of Labor Force Unemployed</td>
            <td class="relocate-td">3.5</td>
            <td class="relocate-td">6.4</td>
        </tr>
        <tr>
            <td class="relocate-td">Idaho % Unemployed</td>
            <td class="relocate-td">3.5</td>
            <td class="relocate-td">6.7</td>
        </tr>
        <tr>
            <td class="relocate-td">U.S. % Unemployed</td>
            <td class="relocate-td">4.6</td>
            <td class="relocate-td">7.3</td>
        </tr>
    </tbody>
</table>