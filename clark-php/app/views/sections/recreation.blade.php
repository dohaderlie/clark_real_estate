<h3 class="relocate-h3"><b>Attractions and Recreation</b></h3>
<a name="recreation"></a>
<h6 class="relocate-h6"><b>Idaho Museum of Natural History</b></h6>
<p class="relocate-p"><a href="tel:2082823317">(208) 282-3317</a> - ISU Campus<br/>
    5th Ave., Pocatello, Idaho<br/>
    <a href="http://imnh.isu.edu">imnh.isu.edu</a></p>
<p class="relocate-p">Exhibits of dinosaur's fossils that lived in southeast Idaho are on display in the museum.  You will see dioramas and murals showing recreation of Idaho during dinosaur times.  Other exhibits of current events are also featured at the museum.</p>
<h6 class="relocate-h6"><b>Pocatello Zoo</b></h6>
<p class="relocate-p"><a href="tel:2082346264">(208) 234-6264</a><br/>
    Ross Park, Pocatello, Idaho<br/>
    <a href="wwww.pocatellozoo.org">www.pocatellozoo.org</a></p>
<p class="relocate-p">Owned by the city of Pocatello, this zoo features a variety of North American wildlife in a natural setting.  It is open to the public with admission and offers special tours and educational programs.</p>
<h6 class="relocate-h6"><b>Replica of Old Fort Hall</b></h6>
<p class="relocate-p"><a href="tel:2082346232">(208) 234-6232</a><br/>
    Upper Level of Ross Park, Pocatello, Idaho<br/>
    <a href="www.forthall.net">www.forthall.net</a></p>
<p class="relocate-p">The original Fort Hall situated in a sheltered bend of the Snake River near the junction of the Blackfoot and Portneuf Rivers.  Constructed by Nathaniel Jarvis Wyeth as a fur trading fort in 1834 and sold to the Hudson's Bay Company in 1837.  The fort was abandoned sometime around 1855.  Most of the valuable itemswere transferred to a post in Montana.  Because the original site could not be acquired, the Replica is located in Ross Park.  Open year round with limited hours.</p>
<h6 class="relocate-h6"><b>Parks</b></h6>
<p class="relocate-p"><a href="tel:2082346232">(208) 234-6232</a> or <a href="tel:2087858622">(208) 785-8622</a></p>
<p class="relocate-p">Several neighborhood parks are scattered through the community.  These parks provide outdoor recreation from boating and baseball to picknicking.</p>
<h6 class="relocate-h6"><b>Ross Park Aquatic Center</b></h6>
<p class="relocate-p"><a href="tel:2082340472">(208) 234-0472</a> - Ross Park, Pocatello, Idaho<br/>
    <a href="www.pocatello.us/parks_rec">www.pocatello.us/parks_rec</a></p>
<p class="relocate-p">Open Memorial Day through Labor Day weekend, this aquatic complex is more than just a pool!  It is a place for everyone to play and have fun.  The zero depth pool with water play element in the middle is great for children.  The lazy river provides for a relaxing tube ride around an activity pool complete with water basketball and a cargo net that patrons can hold on to while crossing lily pads.  The complex also features a water slide and a 25-yard by 25-meter pool.</p>
<h6 class="relocate-h6"><b>Jensen Grove</b></h6>
<p class="relocate-p"><a href="tel:2087858622">(208) 785-8622</a><br/>
    Blackfoot, Idaho</p>
<p class="relocate-p">Owned by the City of Blackfoot, this park offers something for everyone.  It has a large picnic shelter and many picnic tables.  This multi-functional park also has a skate park, many playground stations, a two mile paved walking path, and 55 acres of water for swimming boating, and jet skiing.</p>
<h6 class="relocate-h6"><b>Portneuf Greenway</b></h6>
<p class="relocate-p">Pocatello, Idaho</p>
<p class="relocate-p">This is a 13+ mile Greenway trail system along the Portneuf River, through the City of Pocatello, and to public land trailheads.  It is a wonderful system to walk and enjoythe sites or to simply sit and relax.</p>
<h4 class="relocate-h4"><b>Museums and Theaters</b></h4>
<h6 class="relocate-h6"><b>Bannock County Historical Museum</b></h6>
<p class="relocate-p"><a href="tel:2082330434">(208) 233-0434</a><br/>
    Upper level of Ross Park, Pocatello, Idaho</p>
<p class="relocate-p">Established in 1963, the museum contains exhibits of bead work and baskets made by the early Shoshone-Bannock Indians, railroad related items, farm tool display and other items showing Bannock County's heritage.  Open year round with limited hours.</p>
<h6 class="relocate-h6"><b>Bingham County Historical Museum</b></h6>
<p class="relocate-p"><a href="tel:2087850397">(208) 785-0397</a>, 190 Shilling, Blackfoot, ID</p>
<p class="relocate-p">The museum was once the social center of Blackfoot.  Built in 1905 by the Brown family, this home was built of lava rock from the basement to the second floor.  The top floor is made of lumber with four columns supporting the balcony.  The home is now a museum reflecting life in Blackfoot at the turn of the century.  Open May through September.</p>
<h6 class="relocate-h6"><b>Idaho Potato Museum</b></h6>
<p class="relocate-p"><a href="tel:2087852517">(208) 785-2517</a><br/>
    130 N.W. Main<br/>
    Blackfoot, ID 83221<br/>
    <a href="www.idahopotatomuseum.com">www.IdahoPotatoMuseum.com</a></p>
<p class="relocate-p">The museum is located in the historic Oregon Short Line Railroad Depot and features educational exhibits of antique farm equipment, priceless photos, faring displays and the World's Largest Potato Chip.  Open April through October.</p>
<h6 class="relocate-h6"><b>Shoshone-Bannock Tribal Museum</b></h6>
<p class="relocate-p"><a href="tel:2082379791">(208) 237-9791</a><br/>
    Exit 80 off I-15, 8 miles north of Pocatello</p>
<p class="relocate-p">This museum is located on the Fort Hall Indian Reservation; it offers the history and culture of the Shoshone and Bannock people through words, photographs, artifact and displays.  Open year round with limited hours.</p>
<h4 class="relocate-h4"><b>Arts and Culture</b></h4>
<h6 class="relocate-h6"><b>L.E. and Thelma E Stephens Performing Arts Center</b></h6>
<p class="relocate-p"><a href="tel:2082823595">(208) 282-3595</a><br/>
    <a href="www.isu.edu/stephens">www.isu.edu/stephens</a></p>
<p class="relocate-p">On the campus of Idaho State University, this center is a crown jewel for the art scene from music and dance to theater and lectures.</p>
<h6 class="relocate-h6"><b>Nuart Theatre</b></h6>
<p class="relocate-p"><a href="tel:2087855344">(208) 785-5344</a><br/>
    195 North Broadway Street, Blackfoot, Idaho<br/>
    <a href="www.nuarttheatre.org">www.nuarttheatre.org</a></p>
<p class="relocate-p">Nuart Theatre is on the National Register of Historic Places.  It was built in the 1920s as a theatre to show silent movies.  Today it's owned by the Blackfoot Community Players and is the site of productions held throughout the year.</p>
<h6 class="relocate-h6"><b>Eastern Idaho State Fair</b></h6>
<p class="relocate-p"><a href="tel:2087852480">(208) 785-2480</a><br/>
    <a href="http://idaho-state-fair.com">idaho-state-fair.com</a></p>
<p class="relocate-p">Established in 1902, by a group of local cattlemen to sprint a livestock show and sale.  This fair has been running continuously since its conception.  A Labor Day tradition in Blackfoot, there are activities for all.</p>
<h6 class="relocate-h6"><b>Bannock County Fairgrounds</b></h6>
<p class="relocate-p"><a href="tel:2082371340">(208) 237-1340</a>, Pocatello, Idaho</p>
<p class="relocate-p">Home to soccer fields, racetrack, rodeo arena, grandstand, smaller arenas, horse stalls, exhibit buildings, and RV Park.  This fairgrounds houses both the South Bannock County Fair and the North Bannock County Fair.</p>
<h4 class="relocate-h4"><b>Golf</b></h4>
<h6 class="relocate-h6"><b>Blackfoot Municipal Golf Course</b></h6>
<p class="relocate-p"><a href="tel:2087859960">(208) 785-9960</a><br/>
    Blackfoot, Idaho</p>
<p class="relocate-p">An 18-hole course, bordered on the north by the Snake River and the southwest by Jensen Lake.  It is the home of many major golf tournaments.</p>
<h6 class="relocate-h6"><b>Highland Golf Course</b></h6>
<p class="relocate-p"><a href="tel:2082379922">(208) 237-9922</a>, Pocatello, Idaho</p>
<h6 class="relocate-h6"><b>Riverside Golf Course</b></h6>
<p class="relocate-p"><a href="tel:2082329515">(208) 232-9515</a>, Pocatello, Idaho</p>
<h4 class="relocate-h4"><b>Downhill Skiing</b></h4>
<h6 class="relocate-h6"><b>Pebble Creek</b></h6>
<p class="relocate-p"><a href="tel:2087754452">(208) 775-4452</a>, Inkom, Idaho<br/>
    <a href="www.pebblecreekskiarea.com">www.pebblecreekskiarea.com</a></p>
<p class="relocate-p">Vertical Drop: 2,911 feet; 1 double chair, 2 triple chair; 54 runs.  About 1,100 acres, services include nightly grooming, day lodge, food and lessons.</p>
<h6 class="relocate-h6"><b>Kelly Canyon</b></h6>
<p class="relocate-p"><a href="tel:2085386251">(208) 538-6251</a>, Ririe, Idaho<br/>
    <a href="www.skikelly.com">www.skikelly.com</a></p>
<p class="relocate-p">Vertical drop: 1,000 feet, 4 double chair lifts and two rope tows, 26 runs.  About 640 acres, services include a day lodge and rentals.</p>
<h6 class="relocate-h6"><b>Nordic Skiing and Snowmobiling</b></h6>
<p class="relocate-p"><a href="tel:2083344199">(208) 334-4199</a></p>
<p class="relocate-p">The area has abundant miles and acreage for cross country skiing and snowmobiling.  Idaho Department of Parks and Recreation is an excellent resource for further information</p>
<h6 class="relocate-h6"><b>Mink Creek</b></h6>
<p class="relocate-p">About 15 minutes south of Pocatello.  It has around 15 miles of groomed trails for all levels.</p>
<h6 class="relocate-h6"><b>Trail Canyon</b></h6>
<p class="relocate-p">About 12 minutes northeast of Soda Springs; this area has around 10 miles of groomed trails for all levels.  A warming hut is available.</p>