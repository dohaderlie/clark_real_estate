<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="/contact">Contact Us</a> | <a href="/index">Home</a> | <a href="/properties">Properties</a>
                <h6 class="footer-text">&copy; Clark Real Estate 2015, All Rights Reserved | 1111 Yellowstone Ave. Pocatello, Idaho 83201 | (208) 233-2424</h6>
            </div>
        </div>
    </div>
</footer>