<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/index">Clark Real Estate</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/agent">Staff <span class="sr-only">(current)</span></a></li>
                <li><a href="/mls">Properties</a></li>
                <li><a href="/properties">Properties Outside Idaho</a></li>
                <li><a href="/commercial">Commercial Rentals</a></li>
                <li><a href="/rentals">Residential Rentals</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Relocation <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/relocation">Pocatello Relocation</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Documnets <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../../../public/documents/rentalApp2">Rental Application Page 1</a></li>
                        <li><a href="../../../public/documents/rentalApp1.pdf">Rental Application Page 2</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>