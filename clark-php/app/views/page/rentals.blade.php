@extends('ui/layout')

@section('layout')
<div id="header" class="container-fluid">
    <div class="banner img-responsive"
         style="background-image: url('../../images/fireplace.JPG');">
    </div>
</div>
@include('ui/nav')
<div id="body" class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1>Residential Rentals</h1>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">1800 Ranier</h4><br/>
            <p class="city">Pocatello, Idaho</p><h4 class="price">$1,100.00/month</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Washer/Dryer</th>
                    <th>Heat/AC</th>
                    <th>Garage</th>
                    <th>Comments</th>
                </tr>
                </thead>
                <tr>
                    <td><img src="../../images/srd_front.jpg" alt=""/></td>
                    <td>3</td>
                    <td>2</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>
@include('ui/footer')