@extends('ui/layout')

@section('layout')
<div id="header" class="container-fluid">
    <div class="banner img-responsive"
         style="background-image: url('../../images/main3.jpg');">
    </div>
</div>
@include('ui/nav')
<div id="body" class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1>Search MLS Listings</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <form>
                <div class="form-group">
                    <label for="searchProperties">Search for properties</label>
                    <input type="text" class="form-control" id="searchProperties" placeholder="Enter address">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><a href="http://3655sunridgedr.com/" target="_blank">3655 Sun Ridge Dr.</a></h4><br/>
        <p class="city">Park City, Utah</p><h4 class="price">$2,595,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="../../images/srd_front.jpg" alt=""/></td>
                <td>Elegant home in the Prestigious neighborhood of Royal Oaks</td>
                <td>2002</td>
                <td>9,313</td>
                <td>5</td>
                <td>8</td>
                <td>Indoor Pool & Theater Room</td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">1650 S. Redwood</h4><br/>
        <p class="city">Salt Lake City, Utah</p><h4 class="price">$1,650,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">200 N.E. 800 W.</h4><br/>
        <p class="city">Salt Lake City, Utah</p><h4 class="price">$350,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">3737 S. 900 E.</h4><br/>
        <p class="city">Salt Lake City, Utah</p><h4 class="price">$275,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">3741 S. 900 E.</h4><br/>
        <p class="city">Salt Lake City, Utah</p><h4 class="price">$225,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 7 Bld 1</h4><br/>
        <p class="city"></p><h4 class="price">$64,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 3 Bld 2</h4><br/>
        <p class="city"></p><h4 class="price">$129,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 4 Bld 2</h4><br/>
        <p class="city"></p><h4 class="price">$129,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 6 Bld 2</h4><br/>
        <p class="city"></p><h4 class="price">$129,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 7 Bld 2</h4><br/>
        <p class="city"></p><h4 class="price">$119,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 1 Bld 3</h4><br/>
        <p class="city"></p><h4 class="price">$109,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 2 Bld 3</h4><br/>
        <p class="city"></p><h4 class="price">$109,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 3 Bld 3</h4><br/>
        <p class="city"></p><h4 class="price">$109,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 4 Bld 3</h4><br/>
        <p class="city"></p><h4 class="price">$65,900.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot1 Bld 4</h4><br/>
        <p class="city"></p><h4 class="price">$84,900.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 1 Bld 5</h4><br/>
        <p class="city"></p><h4 class="price">$96,900.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Rainy Creek Lot 2 Bld 5</h4><br/>
        <p class="city"></p><h4 class="price">$64,500.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">11430 County Rd. </h4><br/>
        <p class="city">Kiowa Colorado</p><h4 class="price">$1,995,599.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Dimple Dell Canyon</h4><br/>
        <p class="city"></p><h4 class="price">$6,000,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"></h4><br/>
        <p class="city">Cedar City, Utah</p><h4 class="price">$425,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">200 N. 800 W.</h4><br/>
        <p class="city">Preston, Idaho</p><h4 class="price">$350,000.00</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">2039 Normandy Woods Court</h4><br/>
        <p class="city"></p><h4 class="price">$</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Brief Summary</th>
                <th>Year Built</th>
                <th>Square Feet</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Features</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
</div>
@include('ui/footer')