@extends('ui/layout')

@section('layout')
<div id="header" class="container-fluid">
    <div class="banner img-responsive"
         style="background-image: url('../../images/staff_header.JPG');">
    </div>
</div>
@include('ui/nav')
<div id="body" class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1>Clark Real Estate Staff</h1>
        </div>
    </div>
    <div class="row">
        <!--<div class="col-xs-12 col-md-6">
            <div class="col-md-3 col-md-offset-1">
                <div class="awardsImage execImage" data-exec="gary">
                    <img class="img-thumbnail thumbnail" alt="" src="../../images/garyfam500.jpg" />
                </div>
                <div class="awardsImage execImage" data-exec="karen">
                    <img class="img-thumbnail thumbnail" alt="" src="../../images/karen500.jpg" />
                </div>
            </div>
            <div class="col-xs-12 col-md-3 col-md-offset-3">
                <div class="awardsImage execImage" data-exec="pat">
                    <img class="img-thumbnail thumbnail" alt="" src="../../images/patRawlings500.jpg" />
                </div>
                <div class="awardsImage execImage" data-exec="julie">
                    <img class="img-thumbnail thumbnail" alt="" src="../../images/julie500.jpg" />
                </div>
            </div>
        </div>-->
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="exec_card gary">
                <img class="img-responsive agent_image" src="../../images/garyfam500.jpg"/>
                <h2 class="staff">Gary A. Clark</h2>
                <h4 class="staff_title">Broker/Agent</h4>
                <p> Lorem ipsum dolor sit amet, pellentesque erat dictum lorem dignissim tellus, porta imperdiet sint nisl, et interdum nostra et. Ultricies malesuada nulla scelerisque, a hendrerit nostra. Ultrices neque id, feugiat bibendum amet egestas suscipit nullam id, amet sit nisl torquent pede vestibulum proin, magna dictum at, nec in pretium mauris id arcu quis. Eros rhoncus aliquam vel. Interdum imperdiet ante tortor molestie tellus fusce, aliquam vel. Praesent nonummy sagittis vestibulum porta rhoncus, risus sed iaculis, ut ac blandit vulputate vivamus est, dolor blandit potenti convallis duis, tincidunt aliquam sed volutpat ullamcorper. A non neque sem sapien velit, erat mi tincidunt, sit aliquet porttitor. Vestibulum sed vel mollis, cras consequat tincidunt interdum dolor. Justo sed fermentum felis, tellus erat scelerisque risus nunc sit. Quam donec, aliquam wisi in proin urna. Maecenas convallis leo congue mi, amet convallis pellentesque.</p>
                <p>Lorem ipsum dolor sit amet, pellentesque erat dictum lorem dignissim tellus, porta imperdiet sint nisl, et interdum nostra et. Ultricies malesuada nulla scelerisque, a hendrerit nostra. Ultrices neque id, feugiat bibendum amet egestas suscipit nullam id, amet sit nisl torquent pede vestibulum proin, magna dictum at, nec in pretium mauris id arcu quis. Eros rhoncus aliquam vel. Interdum imperdiet ante tortor molestie tellus fusce, aliquam vel. Praesent nonummy sagittis vestibulum porta rhoncus, risus sed iaculis, ut ac blandit vulputate vivamus est, dolor blandit potenti convallis duis, tincidunt aliquam sed volutpat ullamcorper. A non neque sem sapien velit, erat mi tincidunt, sit aliquet porttitor. Vestibulum sed vel mollis, cras consequat tincidunt interdum dolor. Justo sed fermentum felis, tellus erat scelerisque risus nunc sit. Quam donec, aliquam wisi in proin urna. Maecenas convallis leo congue mi, amet convallis pellentesque.</p>
            </div>
            <div class="exec_card karen">
                <img class="img-responsive agent_image" src="../../images/karen500.jpg"/>
                <h2 class="staff">Karen C. Hill</h2>
                <h4 class="staff_title">Advertising Manager/Bookkeeper</h4>
                <p> Karen C. Hill is the daughter of owner Rodney Clark and has held many positions within the company.  She started working at Clark Real Estate when she was a young teenager.  When she got married she left the company for a few years to support her husbands career.  In 1987 Karen came back to Clark Real Estate full time.  During the last 28 years she has worn many hats for Clark Real Estate. </p>
                <p>Karen was the Rental manager for Pocatello Heights for ten years.  She then moved on to be the Advertising Manager, which she still does.  In the last few years she has also started taking care of the accounts receivable and payable on top of her advertising responsibilities. </p>
            </div>
            <div class="exec_card pat">
                <img class="img-responsive agent_image" src="../../images/patRawlings500.jpg"/>
                <h2 class="staff">Pat Rawlings</h2>
                <h4 class="staff_title">Office Manager/Agent</h4>
                <p>Patricia Rawlings has been a realtor for Clark Real Estate for the last 35 years.  When she first started at Clark Real Estate in 1978 she was hired on to be the bookkeeper, however, the owner, Rodney T. Clark encouraged her and helped her get her real estate license.  Over the years she has worn many hats for Clark Real Estate but overall loves her job.</p>
                <blockquote>
                    <p>&#34;It's always rewarding when I can help someone get into a home or help a seller.  Home ownership is the all-American dream.  It creats good citizens and makes our economy that much better.  Over the years, I have made a decent living doing this but it's more than makeing money, it's about serviceing our clients and making them happy.&#34;</p>
                </blockquote>
                <p>Pat has been honored to have been a two-time GPAR Realtor of the year recipient and to have been able to serve as the President of the GPAR.   </p>
            </div>
            <div class="exec_card julie">
                <img class="img-responsive agent_image" src="../../images/julie500.jpg"/>
                <h2 class="staff">Julie Myers</h2>
                <h4 class="staff_title">Rental Manager</h4>
                <p>Julie Myers is the residential and commercial property manager for Clark Real Estate Co.  She has extensive knowledge in maintenance operations and management of residential and commercial properties.  She has provided management oversight for over 300 properties during her 14 years as a property manager.  Prior to being the property manager she spent 10 years in accounts receivable and payable for Clark Real Estate Co. </p>
                <p>As property manager for Clark Real Estate Co., Julie is personally involved in the daily operations of residential and commercial properties.  She is responsible for directing and supervising staff to insure occupancy and financial goals while maintaining properties in good physical condition and appearance.  In addition she supervises daily management activity such as monitoring the performance of vendors and contractors, operating expenses, customer relations and retention. </p>
            </div>
        </div>
    </div>
</div>
@include('ui/footer')