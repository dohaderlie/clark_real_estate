@extends('ui/layout')

@section('layout')
<div id="header" class="container-fluid">
    <div class="banner img-responsive"
         style="background-image: url('../../images/Oakwood.JPG');">
    </div>
</div>
@include('ui/nav')
<h1>Pocatello Relocation Guide</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            @include('sections/nav')
        </div>
        <div class="col-md-9">
            @include('sections/welcome')
            @include('sections/pocatello')
            @include('sections/education')
            @include('sections/chubbuck')
            @include('sections/recreation')
            @include('sections/general')
        </div>
    </div>
</div>
@include('ui/footer')