@extends('ui/layout')

@section('layout')
<div id="header" class="container-fluid">
    <div class="banner img-responsive"
         style="background-image: url('../../images/Oakwood.JPG');">
    </div>
</div>>
@include('ui/nav')
<div id="body" class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <h1>Commercial Rentals</h1>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">850 E. Young</h4><br/>
        <p class="city">Pocatello, Idaho</p><h4 class="price">$2,625.00/month</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Square Feet</th>
                <th>Heat/AC</th>
                <th>Available</th>
                <th>Comments</th>
            </tr>
            </thead>
            <tr>
                <td><img src="../../images/srd_front.jpg" alt=""/></td>
                <td>2,250</td>
                <td>Gas/Yes</td>
                <td>Now</td>
                <td>Tenants pay utilities</td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">1111 N. 8th Ave.</h4><br/>
        <p class="city">Pocatello, Idaho</p><h4 class="price">$11,258.00/month</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Square Feet</th>
                <th>Heat/AC</th>
                <th>Available</th>
                <th>Comments</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td>9,650</td>
                <td>Gas/Yes</td>
                <td>Now</td>
                <td>Tenants pay utilities</td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">1246 Yellowstone C6</h4><br/>
        <p class="city">Pocatello, Idaho</p><h4 class="price">$150.00/month</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Square Feet</th>
                <th>Heat/AC</th>
                <th>Available</th>
                <th>Comments</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td>150</td>
                <td>Gas/Yes</td>
                <td>Now</td>
                <td>Utilities included</td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">1246 Yellowstone C2 Back</h4><br/>
        <p class="city">Pocatello, Idaho</p><h4 class="price">$600.00/month</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Square Feet</th>
                <th>Heat/AC</th>
                <th>Available</th>
                <th>Comments</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td>600</td>
                <td>Gas/Yes</td>
                <td>Now</td>
                <td>Utilities included</td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">1246 Yellowstone F1</h4><br/>
        <p class="city">Pocatello, Idaho</p><h4 class="price">$8,283.00/month</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Square Feet</th>
                <th>Heat/AC</th>
                <th>Available</th>
                <th>Comments</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td>7,100</td>
                <td>Gas/Yes</td>
                <td>Now</td>
                <td>Tenants pay utilities</td>
            </tr>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">1246 Yellowstone F3</h4><br/>
        <p class="city">Pocatello, Idaho</p><h4 class="price">$1,995.00/month</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive property-table">
            <thead>
            <tr>
                <th>Images</th>
                <th>Square Feet</th>
                <th>Heat/AC</th>
                <th>Available</th>
                <th>Comments</th>
            </tr>
            </thead>
            <tr>
                <td><img src="" alt=""/></td>
                <td>1,485</td>
                <td>Gas/Yes</td>
                <td>Now</td>
                <td>Utilities included</td>
            </tr>
        </table>
    </div>
</div>
</div>
@include('ui/footer')