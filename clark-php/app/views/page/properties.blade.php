@extends('ui/layout')

@section('layout')
<div id="header" class="container-fluid">
    <div class="banner img-responsive"
         style="background-image: url('../../images/properties_header.JPG');">
    </div>
</div>
@include('ui/nav')
<div id="body" class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1>Other Properties</h1>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><a href="http://3655sunridgedr.com/" target="_blank">3655 Sun Ridge Dr.</a></h4><br/>
            <p class="city">Park City, Utah</p><h4 class="price">$2,595,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                    <tr>
                        <th>Images</th>
                        <th>Brief Summary</th>
                        <th>Year Built</th>
                        <th>Square Feet</th>
                        <th>Bedrooms</th>
                        <th>Bathrooms</th>
                        <th>Features</th>
                    </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="../../images/srd_front.jpg" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">3655 Sun Ridge Dr. Park City, Utah $2,595,000.00</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="../../images/SRD/srd1.jpg" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="../../images/SRD/srd2.jpg" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="../../images/SRD/srd3.jpg" alt="...">
                                            </div>
                                            <div>
                                                <p>Built in 2002 and located in the prestigious neighborhood of Royal Oaks, this European inspired, carefully crafted home boasts spectacular panoramic views. Ideal for entertaining, this elegant and spacious home offers high-end upgrades and finishes, including venetian plaster accented walls, built-in book cases, in-laid slate and cherry wood floors, a six car heated garage and heated driveway. Outstanding features include an executive office with fireplace and private bath, gourmet chefs kitchen with state-of-the-art appliances, spacious great room with large, custom fireplace and a formal dining room. A large upper level loft with custom built- cabinetry, spectacular master-suite with fireplace, large master bath and adjoining sitting room and two additional bedrooms. The walk-out lower level offers a theatre room, entertainment bar, large seating area with fire place, two bedrooms with baths, and indoor swimming pool with retracting glass walls for seamless indoor-outdoor living and entertaining.</p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b>Cooling: </b>Other</li>
                                                    <li><b>Heating: </b>Other, Radiant, Oil</li>
                                                    <li><b>Exterior Material: </b>Stone</li>
                                                    <li><b>Roof Type: </b>Metal</li>
                                                    <li><b>Structure Type: </b>Contemporary</li>
                                                    <li><b>School District: </b>Park City</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td>Elegant home in the Prestigious neighborhood of Royal Oaks</td>
                    <td>2002</td>
                    <td>9,313</td>
                    <td>5</td>
                    <td>8</td>
                    <td>Indoor Pool & Theater Room</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">1650 S. Redwood</h4><br/>
            <p class="city">Salt Lake City, Utah</p><h4 class="price">$1,650,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="srd" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">200 N.E. 800 W.</h4><br/>
            <p class="city">Salt Lake City, Utah</p><h4 class="price">$350,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">3737 S. 900 E.</h4><br/>
            <p class="city">Salt Lake City, Utah</p><h4 class="price">$275,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">3741 S. 900 E.</h4><br/>
            <p class="city">Salt Lake City, Utah</p><h4 class="price">$225,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 7 Bld 1</h4><br/>
            <p class="city"></p><h4 class="price">$64,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 3 Bld 2</h4><br/>
            <p class="city"></p><h4 class="price">$129,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 4 Bld 2</h4><br/>
            <p class="city"></p><h4 class="price">$129,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 6 Bld 2</h4><br/>
            <p class="city"></p><h4 class="price">$129,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 7 Bld 2</h4><br/>
            <p class="city"></p><h4 class="price">$119,500.00</h4>
         </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 1 Bld 3</h4><br/>
            <p class="city"></p><h4 class="price">$109,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 2 Bld 3</h4><br/>
            <p class="city"></p><h4 class="price">$109,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 3 Bld 3</h4><br/>
            <p class="city"></p><h4 class="price">$109,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 4 Bld 3</h4><br/>
            <p class="city"></p><h4 class="price">$65,900.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot1 Bld 4</h4><br/>
            <p class="city"></p><h4 class="price">$84,900.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 1 Bld 5</h4><br/>
            <p class="city"></p><h4 class="price">$96,900.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Rainy Creek Lot 2 Bld 5</h4><br/>
            <p class="city"></p><h4 class="price">$64,500.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">11430 County Rd. </h4><br/>
            <p class="city">Kiowa Colorado</p><h4 class="price">$1,995,599.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Dimple Dell Canyon</h4><br/>
            <p class="city"></p><h4 class="price">$6,000,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"></h4><br/>
            <p class="city">Cedar City, Utah</p><h4 class="price">$425,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">200 N. 800 W.</h4><br/>
            <p class="city">Preston, Idaho</p><h4 class="price">$350,000.00</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#srd"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="srd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p></p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">2039 Normandy Woods Court</h4><br/>
            <p class="city"></p><h4 class="price">$</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive property-table">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Brief Summary</th>
                    <th>Year Built</th>
                    <th>Square Feet</th>
                    <th>Bedrooms</th>
                    <th>Bathrooms</th>
                    <th>Features</th>
                </tr>
                </thead>
                <tr>
                    <td><button type="button" class="btn btn-primary btn-lg modal-button" data-toggle="modal" data-target="#normandy"><img class="img-responsive" src="" alt=""/></button></td>
                    <!--Modal -->
                    <div class="modal fade" id="normandy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">2039 Normandy Woods Court Salt Lake City, Utah $</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div class="item">
                                                <img class="img-responsive" src="" alt="...">
                                            </div>
                                            <div>
                                                <p>Down a quiet wooded lane, nestled in gated, grand homes, a beautiful mansion, sophisticated and rich in detail! Gentle, pavered cul-de-sac to curving path to spacious, extreme vaulted, entry foyer, flooded/light-cut glass panes and 2 story windows.  Fabulous thick molding, plantation shutters, marbles, tiles, rich wood floors and 5 exceptional fireplaces.  </p>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                    <li><b> </b></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>
@include('ui/footer')