@extends('ui/layout')

@section('layout')
<div id="header" class="container-fluid">
    <div class="banner img-responsive"
         style="background-image: url('../../images/staff_header.JPG');">
    </div>
</div>
@include('ui/nav')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1>Contact Us</h1>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <p>We value your opinion and we would love to hear from you!  Let us know how we can help you today!</p>
            <br/>
            <br/>
            <p>Clark Real Estate</p>
            <p>1111 Yellowstone Ave.</p>
            <p>Pocatello, Idaho 83201</p>
            <p>Phone: (208) 233-2424</p>
            <p>Email: contact@clarkrealestateco.com</p>
        </div>
        <div class="col-md-6">
            <form>
                <div class="form-group">
                    <label for="fname">First Name</label>
                    <input type="text" class="form-control" id="fname" placeholder="First Name" required="required">
                    <div class="help-block">Your First Name</div>
                </div>
                <div class="form-group">
                    <label for="lname">Last Name</label>
                    <input type="text" class="form-control" id="lname" placeholder="Last Name" required="required">
                    <div class="help-block">Your Last Name</div>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="example@email.com" required="required">
                    <div class="help-block">Your Email Address</div>
                </div>
                <div class="form-group">
                    <label for="number">Phone Number</label>
                    <input type="tel" class="form-control" id="number" placeholder="(555) 555-5555" required="required">
                    <div class="help-block">Your Phone Number</div>
                </div>
                <div class="form-group">
                    <label for="topic">I am contacting you for:</label>
                    <select name="topic" id="topic">
                    <option value="null">(Select)</option>
                    <option value="sales">Sales</option>
                    <option value="services">Services</option>
                    <option value="concerns">Concerns</option>
                    <option value="suggestions">Suggestions</option>
                    <option value="other">Other</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="comments" for="comments">Questions/Comments</label>
                    <textarea name="comments" id="comments" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>

@include('ui/footer')